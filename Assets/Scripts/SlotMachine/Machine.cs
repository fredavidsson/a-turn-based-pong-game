
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlotMachine
{
    public class Machine : MonoBehaviour
    {
        [SerializeField]
        List<Wheel> wheels;

        int[] score = {0, 0, 0};
        Vector2[] stopPostions = new Vector2[3];

        // This action will be invoked when the machine is done running
        public event System.Action onCompleteAction;

        int numOfWheelsStopped = 0; // 3 => all wheels are stopped

        [SerializeField]
        float[] probabilityTable = {.1f, .3f, .6f};

        void Start()
        {
            // generateRandomStopPositions();

            foreach(Wheel wheel in wheels)
            {
                wheel.onDoneSpinningAction += onWheelDoneSpinningAction;
            }

        }

        void OnDestroy()
        {
            foreach(Wheel wheel in wheels)
            {
                wheel.onDoneSpinningAction -= onWheelDoneSpinningAction;
            }

        }

        void OnEnable()
        {
            generateRandomStopPositions();
        }

        void onWheelDoneSpinningAction()
        {
            numOfWheelsStopped += 1;

            if(numOfWheelsStopped == 3)
            {
                if(onCompleteAction != null)
                    onCompleteAction();
                else
                    Debug.Log("Not able to complete this action.");
            }
        }

        public int GetTotalScore()
        {
            int sum = 0;
            foreach(int i in score){
                sum += i;
                
            }
            return sum;
        }

        void generateRandomStopPositions()
        {
            for(int i = 0; i < 3; i++)
            {
                float r = Random.Range(0f, 1f);
                // 10% chance
                if(r < probabilityTable[0])
                {
                    stopPostions[i] = new Vector2(0.0f, 0.4f); 
                    score[i] = 3;
                } 
                
                // 30% chance
                else if (r < probabilityTable[0] + probabilityTable[1])
                {
                    stopPostions[i] = Vector2.zero;
                    score[i] = 2;
                }

                // 60 % chance
                else
                {
                    stopPostions[i] = new Vector2(0.0f, -0.4f);
                    score[i] = 1;
                }
                
            }
        }

        public void Run()
        {
            // currentState = State.Running;
            StartCoroutine(spinEachWheel());
        }

        IEnumerator spinEachWheel()
        {

            yield return new WaitForSecondsRealtime(1.0f);

            for(int i = 0; i < 3; i++)
            {
                Vector2 stop = stopPostions[i];
                wheels[i].setStopPositon(stop); // assign a stop position to the wheel
                // yield return StartCoroutine(); // spin the wheel!
                wheels[i].Spin();
                yield return new WaitForSecondsRealtime(.25f); // add some delay to each of the wheels
            }
        }

    }
}


