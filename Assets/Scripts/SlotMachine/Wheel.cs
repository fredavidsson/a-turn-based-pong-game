using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SlotMachine
{
    public class Wheel : MonoBehaviour
    {
        Coroutine routine;

        public float wheelSpeed = 1.0f;
        public event System.Action onDoneSpinningAction;

        enum State{
            Spinning, Stopped
        }

        State currentState = State.Stopped;
        Vector2 stopPosition = Vector2.zero;


        public int Spins = 0;

        int MaxSpins = 2;


        public void Spin()
        {
            //Debug.Log("wheel spin!");
            currentState = State.Spinning;
            Spins = 0;
            
            if(routine != null)
                StopCoroutine(routine);

            routine = StartCoroutine(RunUntilStop());
            
        }

        public void setStopPositon(Vector2 pos)
        {
            stopPosition = pos;
        }

        public void Stop()
        {
            currentState = State.Stopped;
        }

        public bool HasStopped()
        {
            return currentState == State.Stopped;
        }

        float spring(float from, float to, float time)
        {
            time = Mathf.Clamp01(time);

            // Pow might be expensive on the performance, at least, that is what I remember writing C code. 
            time = (Mathf.Sin(time * Mathf.PI * (.2f + 2.5f*time*time*time)) * Mathf.Pow(1f - time, 2.2f) + time) * (1f + (1.2f * (1f - time)));
            return from + (to - from) * time;
        }

        Vector3 spring(Vector3 a, Vector3 b, float t)
        {
            return new Vector3(spring(a.x, b.x, t), spring(a.y, b.y, t), spring(a.z, b.z, t));
        }

        IEnumerator RunUntilStop()
        {
            Vector2 direction = Vector2.up;
            float speed = wheelSpeed;
            float time = 0f;
            float duration = 1.0f;
            Vector3 position = transform.localPosition;
            Vector2 resetPosition = new Vector3(0f, -1.18f, 0f);

            while(true)
            {
                if(currentState == State.Stopped)
                {
                    time += Time.unscaledDeltaTime;
                    float t = time / duration;
                    transform.localPosition = spring(resetPosition, stopPosition, t);
                    
                    if(t > 1f)
                    {
                        break; // break while loop
                    }
                } else 
                {
                    transform.localPosition = transform.localPosition + (Vector3) direction * Time.unscaledDeltaTime * wheelSpeed;
                }

            
                // Make sure we are looping
                if(transform.localPosition.y >= 1.18)
                {
                    Spins += 1;
                    Vector3 reset = new Vector3(0f, -1.18f, 0f);
                    transform.localPosition = reset;

                    if(Spins == MaxSpins)
                    {
                        currentState = State.Stopped;
                    }
                }

                yield return null;
            }

            yield return new WaitForSecondsRealtime(0.5f);

            if(onDoneSpinningAction != null)
                onDoneSpinningAction();
            else
                Debug.Log("You forgot to add this action.");
        }

    }
}


