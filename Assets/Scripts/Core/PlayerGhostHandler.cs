using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGhostHandler : MonoBehaviour
{
    public bool isVisible = false;

    private Color _defaultColor;

    private bool _isLockedInPosition = true, _isLockedInRotation = true;

    private SpriteRenderer _sprinteRenderer;
    private PlayerAim _playerAim;
    
    

    void Awake()
    {
        _sprinteRenderer = GetComponent<SpriteRenderer>();
        _playerAim = GetComponent<PlayerAim>();
        _defaultColor = _sprinteRenderer.color;
        _sprinteRenderer.color = _defaultColor;
    }

    public void toggleLockPosition()
    {
        _isLockedInPosition = !_isLockedInPosition;
    }

    public void toggleRotation()
    {
        _isLockedInRotation = !_isLockedInRotation;
        _playerAim.ToggleAim();
        
    }

    public void resetAngle()
    {
        // reset rotation
        transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);
    }

    public void toggleVisibility()
    {
        isVisible = !isVisible;

        if(isVisible)
            _sprinteRenderer.color = _defaultColor;
        else
            _sprinteRenderer.color = new Color(1, 1, 1, 0);
    }
    // Update is called once per frame
    void Update()
    {
        if(_sprinteRenderer == null)
            return;

        if(!isVisible)
            return;

        Camera cam = Camera.main;

        if (!_isLockedInRotation)
        {
            
            Vector3 dir = Input.mousePosition - cam.WorldToScreenPoint(transform.position);
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }

        if (_isLockedInPosition)
            return;

        
        Vector2 mousePos = new Vector2();
        mousePos.x = -8.0f; // Make sure paddle is stuck to the left of the screen
        mousePos.y = cam.ScreenToWorldPoint(Input.mousePosition).y;

        transform.position = mousePos;
        
    }
}
