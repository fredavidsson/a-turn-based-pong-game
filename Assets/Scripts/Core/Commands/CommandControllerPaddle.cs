using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;
using Core.Actors;
using Core.UI;

namespace Core.Commands{
    public class CommandControllerPaddle : MonoBehaviour
    {   

        [SerializeField]
        UnityEvent<bool> bottomPanelSetInteractable;

        [SerializeField]
        Ghost ghost;

        [SerializeField]
        Lock inputLocker;

        [SerializeField]
        private Queue<Command> commands = new Queue<Command>();
        private Command currentCmd;
        private Coroutine commandCoroutine;

        enum Stage
        {
            Moving, Rotating, Ready
        }

        Stage currentStage = Stage.Moving;

        void Awake()
        {
            // ghostHandler.transform.position = transform.position;
            ghost.transform.position = transform.position;
            transform.position = new Vector2(-8.0f, 0.0f);
        }

        void switchToStage(Stage stage)
        {
            // Debug.Log("moving from stage " + currentStage + " to stage " + stage);
            currentStage = stage;
        }

        public void ExecuteCommands()
        {
            if(commandCoroutine != null)
                StopCoroutine(commandCoroutine);

            commandCoroutine = StartCoroutine(ProcessCommands());
        }

        public void InputLeftMouseEvent(InputAction.CallbackContext context)
        {
            if(inputLocker.IsLocked())
                return;

            if(context.performed)
            {

                if(currentStage == Stage.Moving)
                {
                    PushMovePaddleCommand();
                    switchToStage(Stage.Rotating);
                } else if (currentStage == Stage.Rotating)
                {
                    PushRotatePaddleCommand();
                    bottomPanelSetInteractable?.Invoke(true);
                    switchToStage(Stage.Ready);
                }
            }
        }

        public void InputRightMouseEvent(InputAction.CallbackContext context)
        {
            if(inputLocker.IsLocked())
                return;
            if(context.performed)
                CancelPreviousCommand();
        }

        private IEnumerator ProcessCommands()
        {
            while(commands.Count > 0)
            {

                currentCmd = commands.Dequeue();
                currentCmd.Execute();

                // Wait until command is finished
                while(!currentCmd.IsFinished)
                {
                    yield return null;
                }

                yield return new WaitForEndOfFrame();
            }

            switchToStage(Stage.Moving);
        }

        void PushMovePaddleCommand()
        {
            Camera cam = Camera.main;
            Vector2 destination = new Vector2();
            
            destination = ghost.transform.position; // new Vector2(mousePos.x, mousePos.y);    
            Actor actor = GetComponent<Actor>();
            commands.Enqueue(new MoveCommand(actor, destination));

        }

        void PushRotatePaddleCommand()
        {
            Camera cam = Camera.main;

            Vector3 mousePosition = cam.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0;
            Actor actor = GetComponent<Actor>();
            commands.Enqueue(new RotateCommand(actor, mousePosition));
        }

        public void PushShootAt(Ball ball)
        {
            commands.Enqueue(new ShootCommand(ball, ghost.transform.right));
        }

        void CancelPreviousCommand()
        {
            if(commands.Count == 0) // dont dequeue if there are no commands.
            {
                return;
            }

            if(currentStage == Stage.Rotating)
            {
                switchToStage(Stage.Moving);
                // currentStage = Stage.Moving;
            }
            
            if(currentStage == Stage.Ready)
            {
                Debug.Log("Cancel");
                bottomPanelSetInteractable?.Invoke(false);
                switchToStage(Stage.Rotating);
            }
            
            commands.Dequeue();
        }
    }
}