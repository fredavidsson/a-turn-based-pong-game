using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;

namespace Core.Commands
{
    internal class RotateCommand : Command
    {

        private readonly Actor _actor;
        private readonly Vector3 _point;

        public RotateCommand(Actor actor, Vector3 point)
        {
            _actor = actor;
            _point = point;
        }

        public override void Execute()
        {
            _actor.RotateTo(_point);
        }

        public override bool IsFinished => _actor.IsDoneRotating;

    }
}
