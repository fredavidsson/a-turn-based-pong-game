using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Commands{
    internal class ShootCommand : Command
    {  

        private readonly Ball ball;
        private Vector2 direction;

        public ShootCommand(Ball ball, Vector2 dir)
        {
            this.ball = ball;
            direction = dir;
        }

        public override void Execute()
        {
            ball.ShootAtDirection(direction);
        }

        public override bool IsFinished => ball.HasBeenShot;

    }
}
