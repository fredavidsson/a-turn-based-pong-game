using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;

namespace Core.Commands{
    internal class MoveCommand : Command
    {
        // private readonly Vector3 _destination;
        private readonly Actor _actor;
        private readonly Vector2 _destination;

        public MoveCommand(Actor actor, Vector2 destination)
        {
            _actor = actor;
            _destination = destination;
        }

        public override void Execute()
        {
            // _moveable.Move(_destination);
            // _actor.MoveToTarget();
            _actor.MoveToDestination(_destination);
            //_actor.Move();
        }

        public override bool IsFinished => _actor.IsDoneMoving;

    }
}