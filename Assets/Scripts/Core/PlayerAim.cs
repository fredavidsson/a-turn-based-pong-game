using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class PlayerAim : MonoBehaviour
{
    LineRenderer _lineRenderer;
    bool _isAiming = false;

    Color _StartColor;
    Color _endColor;

    void Awake()
    {
        _lineRenderer = GetComponent<LineRenderer>();
        _StartColor = _lineRenderer.startColor;
        _endColor   = _lineRenderer.endColor;
        ToggleHide();
    }
    
    void ToggleHide()
    {
        if(_isAiming)
        {
            _lineRenderer.startColor = _StartColor;
            _lineRenderer.endColor   = _endColor;
        } else 
        {
            _lineRenderer.startColor = new Color();
            _lineRenderer.endColor   = new Color();
        }
    }

    public void ToggleAim()
    {
        _isAiming = !_isAiming;
        ToggleHide();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isAiming) return;

        Camera cam = Camera.main;
        Vector3 lookAt = cam.ScreenToWorldPoint(Input.mousePosition);
        lookAt.z = 0.0f;
        _lineRenderer.SetPosition(1, lookAt);

        Vector3 position = transform.position;
        _lineRenderer.SetPosition(0, position);

    }
}
