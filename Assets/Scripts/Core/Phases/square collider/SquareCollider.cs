using UnityEngine;

using Core;

public class SquareCollider : MonoBehaviour
{
    [SerializeField]
    AnimationCurve initialMovingAnimationCurve;

    [SerializeField]
    float initialSpeed = .5f;
    float step = 0f;

    [SerializeField]
    float rotationSpeed = 90f; // 90 deg./s


    enum State
    {
        INITIAL, FLYING
    }

    State currentState = State.INITIAL;


    void Update()
    {
        
        if(currentState == State.INITIAL)
        {
            step += Time.deltaTime * initialSpeed;
            float pos = initialMovingAnimationCurve.Evaluate(step);
            Debug.Log("pos = " + pos);
            transform.localPosition = Vector3.LerpUnclamped(Vector3.zero, new Vector3(2.0f, 0f, 0f), pos);
            
            
            if(pos >= 1f)
            {
                step = 0f;
                currentState = State.FLYING;
            }
        } 
        else if(currentState == State.FLYING)
        {
            step += Time.deltaTime * rotationSpeed;
            step %= 360;
            Vector3 orbit = Vector3.right * 2;
            orbit = Quaternion.Euler(0f, 0f, step) * orbit;

            transform.localPosition = orbit;

        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if(other.gameObject.GetComponent<Ball>() != null)
        {
            Destroy(gameObject);
        }
    }

}
