using System.Collections;
using UnityEngine;

using Core.Actors;
using Core.Stats;
using System.Collections.Generic;

namespace Core.Phases
{
    [CreateAssetMenu]
    public class InitialPhase : Phase
    {

        [SerializeField]
        GameObject enemyPrefab;

        [SerializeField]
        List<Transform> locations;

        GameObject enemyGO;

        Actor actor;
        Health currentHealth;

        enum LocationState
        {
            TOP, MIDDLE, BOTTOM
        }

        LocationState currentLocationState;
        

        void calculateMarkovChainPosition()
        {
            Vector3 newLocation = Vector3.zero;
            float r = Random.Range(0f, 1f);

            // Note that this is a right stochastic matrix
            float[,] transitionMatrix = {
                {.20f, .80f, .00f},
                {.50f, .30f, .20f}, 
                {.00f, .70f, .30f}}; 


            switch (currentLocationState)
            {
                case LocationState.TOP:
                    if(r < transitionMatrix[0, 0])
                    {
                        // Stay
                        Debug.Log("staying in this position");
                    } else
                    {
                        // Move to the middle.
                        currentLocationState = LocationState.MIDDLE;
                        actor?.MoveToDestination(locations[1].position); // 1 is middle
                    }
                    break;
                case LocationState.MIDDLE:
                    if(r < transitionMatrix[1, 2])
                    {
                        // move down
                        currentLocationState = LocationState.BOTTOM;
                        actor?.MoveToDestination(locations[2].position); // 2 is bottom
                    }
                    else if( r < transitionMatrix[1, 1] + transitionMatrix[1, 2])
                    {
                        // Stay
                        Debug.Log("staying in this position");
                    }
                    else
                    {
                        // move up
                        currentLocationState = LocationState.TOP;
                        actor?.MoveToDestination(locations[0].position); // 0 is up.
                    }
                    break;
                case LocationState.BOTTOM:
                    if(r < transitionMatrix[2, 2])
                    {
                        // stay
                        Debug.Log("staying in this position");
                    } else
                    {
                        // move to the middle.
                        currentLocationState = LocationState.MIDDLE;
                        actor.MoveToDestination(locations[1].position); // 1 is middle
                    }
                    break;

                default:
                    break;

            }
        }

        public override IEnumerator Run()
        {
            initialize();
            int tempHP = currentHealth.GetPoints();
            

            while(true)
            {
                if(tempHP > currentHealth.GetPoints())
                {
                    tempHP = currentHealth.GetPoints();
                    Debug.Log("Ouchie");

                    // attempt to go to a new position
                    calculateMarkovChainPosition(); 

                }
                
                if (tempHP == 0)
                {
                    // at 0 we break this phase
                    break;
                } 
            

                yield return null;
            }
        }

        protected override void initialize()
        {
            enemyGO = Instantiate(enemyPrefab);

            currentHealth   = enemyGO.GetComponent<Health>();
            actor           = enemyGO.GetComponent<Actor>();

            // Sets the initial position in the middle.
            actor.transform.position = locations[1].position;
            currentLocationState = LocationState.MIDDLE;

        }

        public override void SetInitialPosition(Vector3 position)
        {
            // Note that we don't really use this method for the 
            // initial phase. Instead, we spawn at the middle position
            // that is already given in the scriptable object.

            // See initialize method.
        }

        public override Vector3 GetPosition()
        {
            return actor.transform.position;
        }

        public override void Disable()
        {
            enemyGO.SetActive(false);
        }
    }
}


