using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Phases
{
    public class PhaseExecutor : MonoBehaviour
    {
        [SerializeField]
        List<Phase> phases;

        Queue<Phase> phaseQueue;

        Phase currentPhase;

        IEnumerator runNextPhase()
        {
            while(true)
            {
                if(phaseQueue.Count > 0)
                {
                    Vector3 oldPosition = getCurrentPhasePosition();

                    currentPhase?.Disable(); // Initially, this is null.
                    currentPhase = phaseQueue.Dequeue();

                    // this is used in the future phases, making
                    // sure that the boss in next phase spawn
                    // in the same location the previous boss was
                    // defeated in. 
                    currentPhase.SetInitialPosition(oldPosition);

                    // run next phase
                    yield return currentPhase.Run();
                } else 
                {
                    break;
                }
                Debug.Log("This phase done running, going to next.");
            }
            Debug.Log("Done running all phases.");
        }

        Vector3 getCurrentPhasePosition()
        {
            Vector3 oldPosition = Vector3.zero;
            if(currentPhase != null)
            {
                oldPosition = currentPhase.GetPosition();
            }
            return oldPosition;
        }

        void Awake() 
        {
            phaseQueue = new Queue<Phase>();
            foreach(Phase phase in phases)
            {
                phaseQueue.Enqueue(phase);
            }
        }

        void Start()
        {
            StartCoroutine(runNextPhase());
        }


    }
}

