using UnityEngine;
using System.Collections;
using Core.Actors;

namespace Core.Phases
{
    public abstract class Phase : ScriptableObject
    {
        public abstract IEnumerator Run();

        public abstract void SetInitialPosition(Vector3 position);

        public abstract Vector3 GetPosition();

        public abstract void Disable();

        protected abstract void initialize();


        
    }
}

