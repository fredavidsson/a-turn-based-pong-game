using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Core.Stats;
using Core.Actors;

namespace Core.Phases
{
    [CreateAssetMenu]
    public class SecondPhase : Phase
    {

        [SerializeField]
        GameObject enemyPrefab, collisonableSquarePrefab;

        [SerializeField]
        List<Transform> locations;

        List<GameObject> collisionableSquareGOs;

        Vector3 initialPosition = Vector3.zero;

        GameObject enemyGO;
        Enemy enemy;

        Actor actor;

        Health hp;

        int tmpHealth = 0;


        enum LocationState
        {
            TOP, BOTTOM, NOT_SET
        }

        LocationState currentState;

        // makes sure that we spawn where the boss got defeated from last position.
        public override void SetInitialPosition(Vector3 lastPosition)
        {
            initialPosition = lastPosition;
        }

        void initializeCollisionableSquare()
        {
            GameObject go = Instantiate(collisonableSquarePrefab, enemyGO.transform);
            go.transform.SetParent(enemyGO.transform);
            // collisionableSquareGOs.Add(go);
            // Debug.Log("Count = " + collisionableSquareGOs.Count);
        }

        void moveToOtherPosition()
        {
            if(currentState == LocationState.TOP)
            {
                currentState = LocationState.BOTTOM;
                actor.MoveToDestination(locations[1].position); // go to bottom
            } 
            // note that we move to the top when currentState is NOT_SET
            else 
            {
                currentState = LocationState.TOP;
                actor.MoveToDestination(locations[0].position); // go to top
            }
        }

        public override IEnumerator Run()
        {
            initialize();
            while(true)
            {  
                if(tmpHealth > hp.GetPoints())
                {
                    moveToOtherPosition();
                    tmpHealth = hp.GetPoints();

                    // spawn collisionable squares!
                    initializeCollisionableSquare();
                }

                if(hp.GetPoints() == 0)
                {
                    Debug.Log("Dead!");
                    break; 
                }

                yield return null;
            }
        }

        protected override void initialize()
        {
            enemyGO = Instantiate(enemyPrefab, initialPosition, Quaternion.identity);
            enemy   = enemyGO.GetComponent<Enemy>();
            actor   = enemyGO.GetComponent<Actor>();
            hp      = enemyGO.GetComponent<Health>();
            tmpHealth = hp.GetPoints();
            currentState = LocationState.NOT_SET;
        }

        public override Vector3 GetPosition()
        {
            return Vector3.zero;
        }

        public override void Disable()
        {
            enemyGO.SetActive(false);
        }
    }

}

