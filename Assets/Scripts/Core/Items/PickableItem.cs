using UnityEngine;
using Core.Stats;

namespace Core.Item
{
public class PickableItem : MonoBehaviour
{
    public virtual string Name()
    {
        return "Unknown item";
    }

    public virtual bool ApplyTo(Trait trait)
    {
        return false;
    }
}
}
