using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemLocation : MonoBehaviour
{
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, .1f);
    }
}
