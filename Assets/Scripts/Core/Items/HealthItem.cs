using Core.Stats;
using UnityEngine;


namespace Core.Item
{
    public class HealthItem : PickableItem
    {
        // [SerializeField]
        // public int restoreAmount {get; set;}
        [SerializeField]
        int restoreAmount = 1;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>

        public override string Name()
        {
            return "Health item";
        }

        public override bool ApplyTo(Trait trait)
        {
            Health health = (Health) trait;
            bool healable = health.Heal(restoreAmount);
            if(healable)
            {
                Destroy(gameObject);
                return true;
            } else
            {
                return false;
            }
        }
    }
}

