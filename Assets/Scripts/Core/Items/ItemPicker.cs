using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;
namespace Core.Item
{
public class ItemPicker : MonoBehaviour
{

    [SerializeField]
    Player player;

    void OnTriggerEnter2D(Collider2D other)
    {
        HealthItem health = other.gameObject.GetComponent<HealthItem>();
        if(health != null)
        {
            player.ConsumeItem(health);
        }

        AttackPowerItem apItem = other.gameObject.GetComponent<AttackPowerItem>();
        if(apItem != null)
        {
            player.ConsumeItem(apItem);
        }


    }
}
}

