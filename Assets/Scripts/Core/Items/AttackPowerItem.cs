using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Core.Actors;
using Core.Stats;
using Core;

namespace Core.Item
{
    public class AttackPowerItem : PickableItem
    {

        [SerializeField]
        int restoreAmount = 1;

        // implement Name here.
        public override string Name()
        {
            return "Attack power item";
        }

        public override bool ApplyTo(Trait trait)
        {
            AttackPower ap = (AttackPower) trait;

            bool restorable = ap.RestoreAP(restoreAmount);
            if(restorable)
            {
                Destroy(gameObject);
                return true;
            }
            return false;
        }
        
    }
}


