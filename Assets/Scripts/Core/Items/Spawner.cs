using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    GameObject healthItemPrefab, attackPowerItemPrefab;

    List<ItemLocation> locations;

    float[] p = {.10f, .15f, .20f}; // probability table

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        locations = new List<ItemLocation>();
        foreach(Transform child in transform)
        {
            ItemLocation location = child.GetComponent<ItemLocation>();
            if(location == null)
            {
                // this shouldn't be happening.
                Debug.LogWarning("Child here is not an ItemLocation component!");
                break;
            }
            locations.Add(location);
        }
    }

    public void OnEventPlayerGoal()
    {
        // probably should de-spawn the items instead.
        SpawnNewItems();
    }

    public void OnEventBallArrival() 
    {
        SpawnNewItems();
    }

    public void SpawnNewItems()
    {
        int amount = numSpawns();
        // Debug.Log("amount = " + amount);
        System.Random rand = new System.Random();
        List<int> indices = new List<int>();

        int i = 0;
        while(i < amount)
        {
            int randomIndex = rand.Next(locations.Count);
            // Debug.Log(randomIndex);
            if(!indices.Contains(randomIndex))
            {
                indices.Add(randomIndex);
                i += 1;
            }
        }


        // Note that we are allowing items to spawn on top
        // of each other on one spawn point.
        for (int j = 0; j < amount; j++)
        {
            ItemLocation itemLocation = locations[indices[j]];

            

            // Probability of getting health item can be calculated as
            // P(HP) = P(GETTING ITEM) * P(ITEM IS HP) 
            //       = (p[0] + p[1] + p[2]) * (0.33) 
            spawnItem(itemLocation, getRandomPrefab()); // More likely to spawn an attack power item.
        }
    }

    void spawnItem(ItemLocation item, GameObject go)
    {
        Instantiate(go, item.transform);
    }

    // 33% chance that the given item will be a health item
    GameObject getRandomPrefab()
    {
        float p = Random.value;
        // Debug.Log("spawn probability = " + p);
        if(p < 0.33)
        {
            return healthItemPrefab;
        }

        return attackPowerItemPrefab;
    }

    // Expected number of spawns on each round can be calculated as
    // E = 3 * p[0] + 2 * p[1] + 1 * p[2] + 0 * (1 - (p[0] + p[1] + p[2]))
    //   = 3 * p[0] + 2 * p[1] + 1 * p[2]
    int numSpawns()
    {
        float r = Random.value;

        if(r < p[0])
        {
            return 3;
        } 
        else if (r < p[0] + p[1])
        {
            return 2;
        }
        else if (r < p[0] + p[1] + p[2])
        {
            return 1;
        }

        
        return 0;
    }

}
