using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Abilities
{
    [CreateAssetMenu]
    public class BasicDamageAbility : BaseAbility
    {
        public int Damage = 1;
        void Awake()
        {
            AbilityType = Type.Ball;
        }

        public override void Activate(Enemy enemy)
        {
            enemy.TakeDamage(Damage);
            IsFinished = true;
        }

    }
}

