using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using SlotMachine;

namespace Core.Abilities
{
    [CreateAssetMenu]
    public class SlotMachineAbility : BaseAbility
    {
        
        [SerializeField]
        GameObject slotMachinePrefab;

        GameObject slotMachineGO;

        Machine slotMachine;

        Enemy enemy;


        void pauseTime()
        {
            Time.timeScale = 0.0f;
        }

        void continueTime()
        {
            Time.timeScale = 1.0f;
        }

        void doDamage()
        {
            int dmg = slotMachine.GetTotalScore();
            // check if our enemy gameobject is not inactive 
            // before taking damage (otherwise it will throw an error)
            if(enemy.isActiveAndEnabled)
                enemy.TakeDamage(dmg); 
            continueTime();

            slotMachine.onCompleteAction -= doDamage;
            Destroy(slotMachineGO);
            IsFinished = true;
        }

        public override void Activate(Enemy enemy)
        {
            
            this.enemy = enemy;
            slotMachineGO = Instantiate(slotMachinePrefab, Vector3.zero, Quaternion.identity);
            slotMachine = slotMachineGO.GetComponent<Machine>();

            // deals damage to the enemy after all animation has completed.
            slotMachine.onCompleteAction += doDamage; 
            slotMachine.Run();
            pauseTime();
        }

    }
}


