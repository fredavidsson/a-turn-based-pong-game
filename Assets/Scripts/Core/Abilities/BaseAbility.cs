using UnityEngine;
using Core.Actors;

namespace Core.Abilities
{
    public class BaseAbility : ScriptableObject
    {
        public string Name, Description;

        public int AttackPowerCost;

        public bool IsFinished = false; // True when ability has been used

        public int ID = 0;

        public enum Type
        {
            Ball, Player, NotSet
        }

        // This Type enum determines where the ability should be attached to. For example,
        // if it is a damaging ability, then it will be attached to the ball and
        // if it is a healing ability, then it will be attached to the player.
        public Type AbilityType = Type.NotSet; 

        public virtual void Activate(){} // Initial can be occuring here
        public virtual void Activate(Enemy enemy){} // damage can occur to the enemy on collision
        public virtual void Activate(Player player){}
    }
}

