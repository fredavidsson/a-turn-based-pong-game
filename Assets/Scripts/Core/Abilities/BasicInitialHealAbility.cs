using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;
using Core.Stats;

namespace Core.Abilities
{
    [CreateAssetMenu]
    public class BasicInitialHealAbility : BaseAbility
    {

        public int heal = 1;

        void Awake()
        {
            AbilityType = BaseAbility.Type.Player;
        }
        

        public override void Activate(Player player)
        {
            player.Heal(heal);
            IsFinished = true;
        }
    }
}

