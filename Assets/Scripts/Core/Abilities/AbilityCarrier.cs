using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;

namespace Core.Abilities
{
    public class AbilityCarrier : MonoBehaviour
    {
        public List<BaseAbility> abilities;

        void Awake()
        {
            abilities = new List<BaseAbility>();
        }
        
        public void AttachAbility(BaseAbility ability)
        {
            abilities.Add(ability);
        }

        private bool findAndRemove(BaseAbility ability, List<BaseAbility> list)
        {
            
            BaseAbility result = list.Find(item => item.ID == ability.ID);

            if(result != null)
            {
                list.Remove(result);
                // Debug.Log("Ability removed " + ability.name);
                return true;
            } else 
            {
                // Debug.Log("Could not remove ability " + ability.name);
                return false;
            }
        }

        public bool DetachAbility(BaseAbility ability)
        {
            return findAndRemove(ability, abilities);
        }

        public void ActivateAbilities(Player player)
        {
            foreach (BaseAbility ability in abilities)
            {
                ability.Activate(player);
            }
            abilities.Clear();
        }

        IEnumerator ActivateAbilities(Enemy enemy)
        {
            foreach(BaseAbility ability in abilities)
            {
                ability.Activate(enemy);
                while(!ability.IsFinished)
                {
                    yield return new WaitForEndOfFrame();
                }
            }
            abilities.Clear(); 
            yield return null;
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            Enemy enemy = other.gameObject.GetComponent<Enemy>();
            if(enemy != null)
            {
                StartCoroutine(ActivateAbilities(enemy));
                return;
            }
        }
    }
}
