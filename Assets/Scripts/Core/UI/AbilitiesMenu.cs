using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Core.Actors;
using Core.Stats;
using Core.Abilities;

namespace Core.UI
{
    public class AbilitiesMenu : MonoBehaviour
    {
        [SerializeField]
        GameObject panel;
        // Image background;

        [SerializeField]
        Transform contentLocation;

        [SerializeField]
        GameObject abilityPanelPrefab;

        
        [SerializeField]
        BaseAbility[] abilites;

        List<AbilityPanel> abilityPanels;
        
        [SerializeField]
        Player player;

        AttackPower playerAttackPower;

        // [SerializeField]
        // UnityEvent UIStatsUpdater;

        void Awake()
        {
            playerAttackPower = player.GetComponent<AttackPower>();
            initializeAbilities();
        }

        void initializeAbilities()
        {

            abilityPanels = new List<AbilityPanel>();
            foreach(BaseAbility ability in abilites)
            {
                GameObject obj = Instantiate(abilityPanelPrefab, contentLocation);
                AbilityPanel panel = obj.GetComponent<AbilityPanel>();
                
                // panel.UpdateListAction.AddListener(RefreshUIStats);
                panel.OnRadioButtonClickAction += updateAbilitiesList;
                panel.SetBuyer(player);
                panel.SetAbility(ability);
                abilityPanels.Add(panel);
            }
        }

        void OnEnable()
        {
            foreach(AbilityPanel panel in abilityPanels)
            {
                panel.OnRadioButtonClickAction += updateAbilitiesList;
            }
        }
        
        void OnDisable()
        {
            foreach(AbilityPanel panel in abilityPanels)
            {
                panel.OnRadioButtonClickAction -= updateAbilitiesList;
            }
        
        }

        // Make abilities non-interractable if we don't have enough points to spend on an ability
        void updateAbilitiesList()
        {
            AttackPower playerAp = playerAttackPower;
            foreach(AbilityPanel abilitypanel in abilityPanels)
            {
                if(abilitypanel.IsBought)
                    continue;

                bool hasEnoughAp = playerAp.CanSpend(abilitypanel.AttackPowerPrice());

                // Don't allow player to interact with the panel if 
                // he doesn't have enough AP to spend on it
                if( !hasEnoughAp && abilitypanel.IsInteractable())
                {
                    abilitypanel.SetInteractable(false);
                } 
                // Allow player to interact with the panel if he
                // has enough AP to spend on that ability.
                else if ( hasEnoughAp && !abilitypanel.IsInteractable())
                {
                    abilitypanel.SetInteractable(true);
                }
            }
        }

        public void OnEventBallArrival()
        {
            ResetRadioButtons();
        }

        public void ResetRadioButtons()
        {
    
            foreach(AbilityPanel ability in abilityPanels)
            {
                ability.ResetRadioButton();
            }
        }

        public void EnableMenu(bool b)
        {
            // background.enabled = b;
            panel.SetActive(b);
            updateAbilitiesList();
        }
    }
}

