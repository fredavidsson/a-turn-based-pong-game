using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Core.Actors;
using Core.Abilities;

public class AbilityPanel : MonoBehaviour
{
    [SerializeField]
    Toggle toggle;

    [SerializeField]
    Text priceTag;

    [SerializeField]
    Text description;

    [SerializeField]
    Text label;

    BaseAbility ability;

    Player player;

    public event System.Action OnRadioButtonClickAction;

    public bool IsBought = false;

    public void SetAbility(BaseAbility ab)
    {
        ability = ab;
    }

    public void SetBuyer(Player p)
    {
        player = p;
    }

    void Awake()
    {
        Debug.Log("Added ability: " + ability.Name);
        priceTag.text = ability.AttackPowerCost + " AP";
        description.text = ability.Description;
        label.text = ability.Name;
    }

    public int AttackPowerPrice()
    {
        return ability.AttackPowerCost;
    }

    public void ResetRadioButton()
    {
        toggle.SetIsOnWithoutNotify(false);
        IsBought = false;
    }

    public bool IsInteractable()
    {
        return toggle.IsInteractable();
    }

    public void SetInteractable(bool b)
    {
        toggle.interactable = b;
    }

    public void OnRadioButtonClick()
    {
        // spend attack power points if this has not been bought
        if(IsBought == false) 
        {
            if(player.BuyAbility(ability))
            {   
                IsBought = true;

                // Make sure that we can use the ability by setting this to false.
                ability.IsFinished = false; // should this be here?
            }
            else
            {
                Debug.Log("Player attempted to buy an ability but didn't have enough AP.");
            }
        } 
        // Refund attack power points if this has been bought already
        else if (IsBought == true) 
        {
            if(player.RefundAbility(ability))
            {
                IsBought = false;
            }    
            else
            {
                Debug.Log("Player was not able to refund ability because it was not purchased.");
            }
        }
        
        // Refresh the list when we check a box.
        OnRadioButtonClickAction ? .Invoke();
        // UpdateListAction ? .Invoke();
    }

}
