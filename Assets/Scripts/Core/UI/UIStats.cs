using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;
using Core.Stats;

namespace Core.UI
{
    public class UIStats : MonoBehaviour
    {

        [SerializeField]
        Player player;

        [SerializeField]
        Transform healthBarContainer, attackPowerBarContainer;

        [SerializeField]
        GameObject statsBarPrefab;

        List<GameObject> hpListHealthBars, apListAttackPowerBars;

        const int maxBars = 13;

        int tempHP, tempAP;

        void Awake()
        {
            hpListHealthBars = new List<GameObject>();
            apListAttackPowerBars = new List<GameObject>();

            tempHP = maxBars;
            tempAP = maxBars;
        }

        void Start()
        {
            instantiateList(apListAttackPowerBars, attackPowerBarContainer);
            instantiateList(hpListHealthBars, healthBarContainer);

            UpdateAttackPowerUI();
            UpdateHealthUI();
        }

        void instantiateList(List<GameObject> list, Transform parent)
        {
            for (int i = 0; i < maxBars; i++)
            {
                list.Add(Instantiate(statsBarPrefab, parent));
            }
        }

        // todo: refactor these ugly codes, they don't belong here. ------------------------
        IEnumerator ScaleAndFadeOut(SpriteRenderer sprite)
        {
            // yield return new WaitForSeconds(0.1f);
            bool animating = true;
            float t = 0.0f;
            float tMax = 0.10f;

            Color c0 = new Color(1.0f, .92549f, .83921f); // sprite.color;
            Color c1 = c0;
            c1.a = 0.0f;

            Vector3 scale = sprite.transform.localScale;
            Vector3 targetScale = scale * 1.25f;

            //Debug.Log("Fading");
            while(animating)
            {
                t += Time.unscaledDeltaTime;
                // changeAlphaColor(sprite, amount);
                sprite.color = Color.Lerp(c0, c1, t / tMax);
                sprite.transform.localScale = Vector3.Lerp(scale, targetScale, t / tMax);

                if(tMax - t < 0){
                    animating = false;
                    // Debug.Log("Done animating!!");
                }

                yield return null;
            }
            // reset scale
            sprite.transform.localScale = scale;
            // Debug.Log("Done fading");
        }

        // todo: refactor these ugly codes, they don't belong here. ------------------------
        IEnumerator ScaleAndFadeIn(SpriteRenderer sprite)
        {
            // yield return new WaitForSeconds(0.1f);
            bool animating = true;
            float t = 0.0f;
            float tMax = 0.10f;

            Color c0 = new Color(1.0f, .92549f, .83921f); // sprite.color;
            Color c1 = new Color(.8156f, .5058f, .35901f);

            Vector3 targetScale = sprite.transform.localScale;
            Vector3 scale = sprite.transform.localScale * 1.25f;

            //Debug.Log("Fading");
            while(animating)
            {
                t += Time.unscaledDeltaTime;
                // changeAlphaColor(sprite, amount);
                sprite.color = Color.Lerp(c0, c1, t / tMax);
                sprite.transform.localScale = Vector3.Lerp(scale, targetScale, t / tMax);

                if(tMax - t < 0){
                    animating = false;
                    // Debug.Log("Done animating!!");
                }

                yield return null;
            }
            // reset scale
            // sprite.transform.localScale = scale;
            // Debug.Log("Done fading");
        }

        void changeAlphaColor(SpriteRenderer s, float amount)
        {
            Color tmp = s.color;
            tmp.a = amount; 
            s.color = tmp;
        }

        void reducePoints(int from, int amount, List<GameObject> list)
        {
            
            for (int i = 0; i < amount; i++)
            {
                int index = Mathf.Clamp(from - (1 + i), 0, maxBars); // prevents index from going to the negatives.
                
                SpriteRenderer sprite = list[index].GetComponent<SpriteRenderer>();
                StartCoroutine(ScaleAndFadeOut(sprite));
                // changeAlphaColor(sprite, 0.25f);
            }
        }

        void increasePoints(int from, int amount, List<GameObject> list)
        {
            
            for (int i = 0; i < amount; i++)
            {
                SpriteRenderer sprite = list[from + i].GetComponent<SpriteRenderer>();
                // changeAlphaColor(sprite, 1.0f);
                StartCoroutine(ScaleAndFadeIn(sprite));
            }
        }

        public void UpdateHealthUI()
        {
            int currentHp = player.GetCurrentHealthPoints();

            if(currentHp < tempHP) 
            {
                int amount = tempHP - currentHp;
                reducePoints(tempHP, amount, hpListHealthBars);
            }
            else if (currentHp > tempHP)
            {
                int amount = currentHp - tempHP;
                increasePoints(tempHP, amount, hpListHealthBars);
            }
            tempHP = currentHp;
        }

        public void UpdateAttackPowerUI()
        {
            int currentAp = player.GetCurrentAttackPowerPoints();
            // If the actor has less AP than a moment ago
            // then reduce number of visual attack points
            if(currentAp < tempAP)
            {
                int amount = tempAP - currentAp;
                reducePoints(tempAP, amount, apListAttackPowerBars);
            } 
            // If the actor has more AP than a moment ago
            // then increase number of visual attack points
            else if (currentAp > tempAP)
            {
                int amount = currentAp - tempAP;
                increasePoints(tempAP, amount, apListAttackPowerBars); 
            }
            tempAP = currentAp;
        }
    }
}


