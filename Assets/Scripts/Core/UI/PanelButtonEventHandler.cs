using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Core.States;

namespace Core.UI
{
    public class PanelButtonEventHandler : MonoBehaviour
    {

        [SerializeField] AbilitiesMenu abilitiesMenu;
        [SerializeField] BottomPanel bottomPanel;

        // The abilities button bottom of the screen
        public void OnButtonAbilities()
        {
            abilitiesMenu.EnableMenu(true);
            bottomPanel.SetButtonsInteractable(false);
        }

        // Disable the buttons on the bottom of the screen
        // when executing.
        public void OnButtonExecute()
        {
            bottomPanel.SetButtonsInteractable(false);
        }

        // This is the button inside AbilityMenu
        public void OnButtonDone()
        {
            abilitiesMenu.EnableMenu(false);
            bottomPanel.SetButtonsInteractable(true);
        }
    }
}
