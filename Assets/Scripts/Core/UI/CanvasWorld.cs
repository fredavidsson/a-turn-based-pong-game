using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Core.UI
{
    public class CanvasWorld : MonoBehaviour
    {

        [SerializeField] DamageText DamageEnemyTextPrefab;

        public void DisplayDamageText(string text, Vector2 location)
        {
            // Debug.Log("creating DisplayDamageText");
            DamageText txt = Instantiate(DamageEnemyTextPrefab, location, Quaternion.identity, this.transform);
            txt.SetPosition(location);
            txt.ShowText(text);
        }

    }

}

