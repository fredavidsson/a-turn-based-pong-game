using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Core.UI
{
    public class DamageText : MonoBehaviour
    {
        Text _text;
        Text _shadowText;

        Color _fadeToColor;

        public Color _defaultColorText, _defaultColorShadow;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            _shadowText = GetComponentsInChildren<Text>()[0];
            _text = GetComponentsInChildren<Text>()[1];

            _defaultColorShadow = _shadowText.color;
            _defaultColorText   = _text.color;
            // hideText();
        }

        public void SetPosition(Vector2 location)
        {
            transform.position = location;
        }

        public void ShowText(string txt)
        {
            setText(txt);
            Vector2 location = transform.position + new Vector3(0f, 0.5f, 0f);
            StartCoroutine(fadeAnimateToLocation(location));
        }

        private void displayText()
        {
            _text.color = _defaultColorText;
            _shadowText.color = _defaultColorShadow;
        }


        private void setText(string text)
        {
            _shadowText.text = text;
            _text.text = text;
        }

        private IEnumerator fadeAnimateToLocation(Vector2 destination)
        {
            bool animating = true;
            float t0 = 0.0f;
            float t1 = 2.0f;

            // Vector2 oldPosition = transform.position + new Vector3(0f, 0.5f, 0f);

            displayText();
            // yield return new WaitForSeconds(0.1f); // wait for a moment before fading away 

            while(animating)
            {

                // fade out
                if(t0 <= t1)
                {
                    transform.position = Vector2.Lerp(transform.position, destination, t0 / t1);
                    _text.color = Color.Lerp(_defaultColorText, _fadeToColor, t0 / t1);
                    _shadowText.color = Color.Lerp(_defaultColorShadow, _fadeToColor, t0 / t1);
                } 
                else if(t0 > t1)
                {
                    animating = false;
                }


                t0 += Time.deltaTime;
                yield return null;
            }
            Destroy(gameObject);
            yield return null;
        }
    }
}

