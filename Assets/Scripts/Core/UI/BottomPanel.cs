using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

namespace Core.UI
{
    public class BottomPanel : MonoBehaviour
    {
        [SerializeField]
        private Button executeButton, abilityButton;

        public bool AreButtonsInteractable()
        {
            return executeButton.interactable && abilityButton.interactable;
        }

        public void SetButtonsInteractable(bool b)
        {
            executeButton.interactable = b;
            abilityButton.interactable = b;
        }

        public void OnEventInteractable(bool b)
        {
            SetButtonsInteractable(b);
        }
    }
}


