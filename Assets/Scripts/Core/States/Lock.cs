using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{

    bool locked = false;

    public void SetLocked(bool b)
    {
        locked = b;
    }

    public bool IsLocked()
    {
        return locked;
    }

}
