using UnityEngine;
using UnityEngine.InputSystem;

namespace Core.States.Ball 
{
    public class BallStateManager : MonoBehaviour
    {
        BallBaseState _currentState;
        public BallWaitState WaitState = new BallWaitState();
        public BallFlyState FlyState = new BallFlyState();

        void Start()
        {
            _currentState = FlyState;
            _currentState.EnterState(this);
        }

        public void HandleInputBall(InputAction.CallbackContext context)
        {
            bool ready = _currentState.Equals(WaitState);
            if(context.performed && ready)
            {
                SwitchState(FlyState);
            }
        }

        void Update()
        {
            
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if(other.tag != "StateCollision")
            {
                return;
            }
            SwitchState(WaitState);
        }

        void SwitchState(BallBaseState state)
        {
            _currentState = state;
            state.EnterState(this);
        }

    }
}