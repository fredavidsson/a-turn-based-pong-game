using UnityEngine;

namespace Core.States.Ball 
{
    public abstract class BallBaseState
    {
        public abstract void EnterState(BallStateManager ball);
        // public abstract void UpdateState(BallStateManager ball);
        public abstract void OnTriggerEnter2D(BallStateManager ball, Collider2D other);
    }

}