using UnityEngine;

namespace Core.States.Ball
{
    public class BallWaitState : BallBaseState
    {

        bool _shouldPause = false;

        public override void EnterState(BallStateManager ball)
        {
            if(_shouldPause)
                Time.timeScale = 0.0f;
            
            _shouldPause = !_shouldPause;
        }
        /*
        public override void UpdateState(BallStateManager ball)
        {
            
        }
        */
        public override void OnTriggerEnter2D(BallStateManager ball, Collider2D other)
        {
            
        }



    }

}

