using System.Collections;
using System.Collections.Generic;
using Core.Commands;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Core.States
{
public class BattleHandler : MonoBehaviour
{
    private enum State 
    {
        WaitingForPlayer, Busy, PlayerStartingGame
    }

    private State currentState;
    
    // private bool keysLocked;
    [SerializeField] CommandControllerPaddle cmdCtrlPaddle;

    [SerializeField] Core.Ball ball;
    [SerializeField] Transform player;

    // This is triggered when ball enters the goal at the player
    public void OnEventPlayerGoal()
    {        
        // Restore ball position
        ball.HasBeenShot = false;
        switchState(State.PlayerStartingGame);
    }

    public void OnEventBallArrival()
    {
        switchState(State.WaitingForPlayer);
        togglePauseTime();
    }

    public void ContinueBattle()
    {
        if(currentState == State.WaitingForPlayer)
        {
            // Resume the time and execute the command sequence when ready
            togglePauseTime();
        } 
        else if (currentState == State.PlayerStartingGame)
        {
            cmdCtrlPaddle.PushShootAt(ball);
        }
        cmdCtrlPaddle.ExecuteCommands();
        switchState(State.Busy);
    }

    public void OnButtonExecute()
    {
        ContinueBattle();
    }

    void Awake()
    {
        currentState = State.PlayerStartingGame;
    }

    void togglePauseTime()
    {
        if(Time.timeScale < 1.0f)
            Time.timeScale = 1.0f;
        else
            Time.timeScale = 0.0f;
    }

    void switchState(State s)
    {
        // Debug.Log("State changed to " + s);
        currentState = s;
    }

    // this updates only position of the ball at the paddle if ball has not been shot yet
    // from player's paddle position
    void UpdateBallPositionOnPlayer()
    {
        if(ball.HasBeenShot && currentState != State.PlayerStartingGame)
            return;
        ball.transform.position = player.right * 0.5f + player.position;
    }

    void Update()
    {
        // This updates the ball position at the paddle at the start of a game
        UpdateBallPositionOnPlayer();



    }
}

}

