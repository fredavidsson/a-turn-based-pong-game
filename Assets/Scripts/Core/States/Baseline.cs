using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

using Core;

public class Baseline : MonoBehaviour
{
    [SerializeField]
    UnityEvent eventOnBallArrival;

    [SerializeField]
    UnityEvent eventOnBallSent;

    enum Arrival
    {
        FromPlayer, FromEnemy
    }

    Arrival ballNextArrival = Arrival.FromPlayer;
    void OnTriggerEnter2D(Collider2D other)
    {

        Ball ball = other.gameObject.GetComponent<Ball>();
        if(ball == null)
            return;

        if(ballNextArrival == Arrival.FromEnemy)
        {
            eventOnBallArrival ? .Invoke();
            ballNextArrival = Arrival.FromPlayer;
        } 
        else if(ballNextArrival == Arrival.FromPlayer)
        {
            eventOnBallSent ? .Invoke();
            ballNextArrival = Arrival.FromEnemy;
        }

    }

}
