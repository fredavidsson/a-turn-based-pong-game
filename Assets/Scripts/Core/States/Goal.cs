using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.States
{
    public class Goal : MonoBehaviour
    {
        [SerializeField]
        UnityEvent triggerEvent;

        void Awake()
        {

        }

        void OnTriggerEnter2D(Collider2D other)
        {
            triggerEvent.Invoke();
        }
    }
}


