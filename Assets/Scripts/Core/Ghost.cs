using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Ghost : MonoBehaviour
{

    [SerializeField]
    Lock inputLocker;

    [SerializeField]
    float maxHeight = 1.0f, minHeight = 1.0f;

    SpriteRenderer sprite;
    enum Stage 
    {
        Moving, Rotating, Waiting
    }

    Stage currentStage;

    // OrdinaryDifferentialEquation ode;

    void Awake()
    {
        // ode = new OrdinaryDifferentialEquation(1.0f, .5f, 0.5f, Vector2.zero);
        sprite = GetComponent<SpriteRenderer>();
    }
    
    // Update is called once per frame
    void Update()
    {
        if(inputLocker.IsLocked())
            return;

        if(currentStage == Stage.Moving)
            moveToMousePosition();
        else if(currentStage == Stage.Rotating)
            rotateTowardsMousePosition();
    }

    void moveToMousePosition()
    {
        Camera cam = Camera.main;

        Vector2 mousePos = new Vector2();
        mousePos.x = -8.0f;// -8.0f Make sure paddle is stuck to the left of the screen
        mousePos.y = cam.ScreenToWorldPoint(Input.mousePosition).y;

        if(mousePos.y > maxHeight)
        {
            mousePos.y = maxHeight;
        } else if(mousePos.y < minHeight)
        {
            mousePos.y = minHeight;
        }
    
        // transform.position = ode.Update(Time.deltaTime, mousePos, ((Vector2) transform.position - mousePos) * 4f );

        transform.position = mousePos;
    }

    void rotateTowardsMousePosition()
    {
        Camera cam = Camera.main;

        Vector3 dir = Input.mousePosition - cam.WorldToScreenPoint(transform.position);
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void resetAngle()
    {
        transform.rotation = Quaternion.AngleAxis(0.0f, Vector3.forward);
    }

    Stage goToNextStage()
    {
        switch(currentStage)
        {
            case Stage.Moving:
                return Stage.Rotating;
            case Stage.Rotating:
            default:
                return Stage.Waiting;
        }
    }

    Stage goToPreviousStage()
    {
        switch(currentStage)
        {
            case Stage.Waiting:
                resetAngle();
                return Stage.Rotating;
            case Stage.Rotating:
            default:
                return Stage.Moving;
        }
    }
    
    void changeOpacity(SpriteRenderer sprite, float amount)
    {
        Color tmp = sprite.color;
        tmp.a = amount;
        sprite.color = tmp;
    }

    public void InputLeftMouseEvent(InputAction.CallbackContext context)
    {
        if (inputLocker.IsLocked())
            return;
        if(context.performed)
            currentStage = goToNextStage();
    }

    public void InputRightMouseEvent(InputAction.CallbackContext context)
    {
        if(inputLocker.IsLocked())
            return;

        if(context.performed)
            currentStage = goToPreviousStage();

    }

    public void OnEventBallArrival()
    {
        ShowGhost();
    }

    public void OnEventPlayerGoal()
    {
        ShowGhost();
    }

    public void OnButtonExecute()
    {
        HideGhost();
    }

    public void ShowGhost()
    {
        resetAngle();
        currentStage = Stage.Moving;
        changeOpacity(sprite, 0.25f);
        inputLocker.SetLocked(false);
    }

    public void HideGhost()
    {
        currentStage = Stage.Waiting;
        changeOpacity(sprite, 0.0f);
        inputLocker.SetLocked(true);
    }
}
