using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wiggle : MonoBehaviour
{
        [Range(0.01f, 50.0f)] // avoid division by zero
        [SerializeField]
        float duration = 0.5f;

        [SerializeField]
        float amplitude = 0.025f, rotationAmount = 0.025f;


        IEnumerator runEnumerator()
        {
            float t = 0f;
            float r = 0.0f;
            float z = transform.localPosition.z;
            Vector3 temp = transform.localPosition;
            
            while(t < duration)
            {
                t += Time.unscaledDeltaTime;
                r = Mathf.Lerp(amplitude, 0, t / duration);
                float x = Random.Range(-r, r);
                float y = Random.Range(-r, r);

                transform.localPosition = new Vector3(x, y, z);
                transform.localEulerAngles = new Vector3(0f, 0f, x * rotationAmount * Mathf.Rad2Deg);

                yield return null;
            }
            transform.localPosition = temp;
            transform.localEulerAngles = Vector3.zero;
            
            
        }

        public void OnEventWiggleThis()
        {
            StartCoroutine(runEnumerator());
        }
}
