using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrdinaryDifferentialEquation 
{
    Vector2 xPrevious; // previous input
    Vector2 y, yDirection; // current state
    private float A, B, C;

    // f    = frequency
    // xi   = dampening
    // r    = reaction speed
    // x0   = initial condition

    // This used to be an ODE, it's now a PDE!
    public OrdinaryDifferentialEquation(float f, float xi, float r, Vector2 x0)
    {
        A = xi / (Mathf.PI * f);
        B = 1 / ((2 * Mathf.PI * f) * (2 * Mathf.PI * f));
        C = r * xi / (2 * Mathf.PI * f);

        xPrevious = x0;
        y = x0;
        yDirection = Vector2.zero;
    }

    // dt = delta time
    // x = current position
    // xDirection = the moving direction
    public Vector2 Update(float dt, Vector2 x, Vector2 xDirection)
    { 
        y = y + dt * yDirection;
        yDirection = yDirection + dt * (x + C * xDirection - y - A * yDirection) / B;

        return y;
    }

}
