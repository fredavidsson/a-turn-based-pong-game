using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Physics
{
    [RequireComponent(typeof(LineRenderer))]
    public class RaycastAim : MonoBehaviour
    {
        [SerializeField] int numReflections = 1;
        [SerializeField] float maxLength = 50f;

        private LineRenderer _lineRenderer;
        private Ray _ray;
        private RaycastHit2D _hit;
        private Vector2 _direction;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        void LateUpdate()
        {
            _ray = new Ray(transform.position, transform.right);
            _lineRenderer.positionCount = 1;
            _lineRenderer.SetPosition(0, transform.position);

            float remainingLength = maxLength;

            for(int i = 0; i < numReflections; i++)
            {
                _hit = Physics2D.Raycast(_ray.origin, _ray.direction, remainingLength);

                if(_hit)
                {
                    _lineRenderer.positionCount += 1;
                    _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, _hit.point);

                    remainingLength -= Vector2.Distance(_ray.origin, _hit.point);

                    // _ray = new Ray(_hit.point, Vector2.Reflect(_ray.direction, _hit.normal));
                    // We get flickering problems with above code due to collision problems, but
                    // adding a small distance away from the collision object fixes the problem
                    Vector2 dir = Vector2.Reflect(_ray.direction, _hit.normal);
                    _ray = new Ray(_hit.point + dir * 0.01f, dir);
                }
                else
                {
                    _lineRenderer.positionCount += 1;
                    _lineRenderer.SetPosition(_lineRenderer.positionCount - 1, _ray.origin + _ray.direction * remainingLength);
                }

            }
        }
        
    }
}
