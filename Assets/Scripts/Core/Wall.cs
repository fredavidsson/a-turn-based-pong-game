using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Physics
{
    public class Wall : MonoBehaviour
    {
        void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.localPosition, transform.right);
        }
    }

}

