using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostEffectExecuter : MonoBehaviour
{

    PostProcessVolume volume;
    Vignette vignette;

    enum State
    {
        ANIMATING, DONE
    }

    State currentState = State.DONE;

    float step = 0f;
    

    [SerializeField]
    float duration = 1.0f;

    [SerializeField]
    Color healthColorPickup, attackPowerColorPickup;

    void Awake()
    {
        volume = GetComponent<PostProcessVolume>();
        volume.profile.TryGetSettings(out vignette); // change some colors later.

        volume.weight = 0f;
    }


    public void OnEventPickingUpHealth()
    {
        vignette.color.Override(healthColorPickup);
        resetAnimation();
    }

    public void OnEventPickingUpAp()
    {
        vignette.color.Override(attackPowerColorPickup);
        resetAnimation();
    }

    void resetAnimation()
    {
        currentState = State.ANIMATING;
        step = 0f;
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(currentState == State.ANIMATING)
        {
            step += Time.deltaTime;
            volume.weight = Mathf.Lerp(1f, 0f,  step / duration);

            if(step > duration)
            {
                currentState = State.DONE;
            }
        }
    }

}
