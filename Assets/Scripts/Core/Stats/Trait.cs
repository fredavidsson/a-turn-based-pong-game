using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Stats
{
    public class Trait : MonoBehaviour
    {
        [SerializeReference]
        protected int Points = 1;
        public int MaxPoints = 13;

        public int GetPoints()
        {
            return Points;
        }

    }
}


