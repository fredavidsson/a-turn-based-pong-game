using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Stats
{
    public class AttackPower : Trait
    {

        // returns true if there is enough AP to spend
        public bool CanSpend(int amount)
        {
            if(Points - amount >= 0)
            {
                return true;
            }

            return false;
        }

        public void SpendAP(int amount)
        {
            if(!CanSpend(amount))
            {
                Debug.LogWarning("Spent too much AP!");
            }
                
            Points -= amount;
        }

        // TODO
        // Potential bug can occur here, we are allowed 
        // to restore more AP than allowed.
        public void RefundAP(int amount)
        {
            Points += amount;
        }

        // returns true if able to restore AP
        public bool RestoreAP(int amount)
        {
        
            if(Points == MaxPoints){
                return false;
            }

            Points += amount;

            // Don't exceed maximum amount of AP
            if(Points > MaxPoints)
            {
                Points = MaxPoints;
            }

            return true;
        }

    }
}


