using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Stats
{
    public class Health : Trait
    {

        // returns true if at zero health or less (death).
        public bool TakeDamage(int amount)
        {
            if(Points >= 1)
            {
                Points -= amount;

                // Don't allow health to go below 0.
                if(Points < 0)
                {
                    Points = 0;
                }
            }

            
            return Points < 1;
        }

        // returns true if we are able to heal 
        // This prevents health potions from being picked up
        // if we are at max health
        public bool Heal(int amount)
        {
            if(Points == MaxPoints)
            {
                return false;
            }

            Points += amount;

            if(Points > MaxPoints)
            {
                Points = MaxPoints;
            }

            
            return true; 
        }

    }
}

