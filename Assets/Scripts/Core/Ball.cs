using UnityEngine;
using Core.UI;
using Core.Actors;

namespace Core
{
    public class Ball : MonoBehaviour
    {
        public float force = 200.0f;
        private Rigidbody2D rigidBody;
        public Vector2 OldVelocity = new Vector2(0, 0);
        public bool HasBeenShot  = false;
        [SerializeField]
        CanvasWorld canvasWorld;

        void Awake()
        {
            rigidBody = GetComponent<Rigidbody2D>();
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            Player player = other.gameObject.GetComponent<Player>();
            Vector2 surfaceNormal = other.contacts[0].normal;
            Vector2 newVelocity = Vector2.zero;

            // player handles this collision
            if(player != null)
                return;

            newVelocity = Vector2.Reflect(OldVelocity, surfaceNormal);
            rigidBody.velocity = newVelocity;
        }

        // this happens just before velocity changes
        void FixedUpdate()
        {
            OldVelocity = rigidBody.velocity;
        }

        public void ShootAtDirection(Vector2 direction)
        {
            // remove the velocity from the object before applying force
            rigidBody.velocity  = Vector2.zero; 
            rigidBody.AddForce(direction.normalized * force);
            HasBeenShot = true;
        }

    }
}

