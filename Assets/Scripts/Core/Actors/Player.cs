using UnityEngine;
using Core.Stats;
using UnityEngine.Events;
using Core.Item;
using Core.Abilities;

namespace Core.Actors
{
    public class Player : Actor
    {
        Health health;
        AttackPower attackPower;

        [SerializeField]
        AbilityCarrier ballAbilityCarrier;
        AbilityCarrier playerAbilityCarrier;

        [SerializeField]
        UnityEvent UIUpdateHealthStats, UIUpdateAttackPowerStats;

        [SerializeField]
        UnityEvent OnEventPickingUpHealth, OnEventPickingUpAp;

        [SerializeField]
        UnityEvent WiggleSprite;

        void Awake() 
        {
            health = GetComponent<Health>();
            attackPower = GetComponent<AttackPower>();
            playerAbilityCarrier = GetComponent<AbilityCarrier>();
        }

        public void OnEventPlayerGoal()
        {
            TakeDamage(1);
        }

        public void OnEventBallSent()
        {
            activatePlayerAbilities();
        }

        public void TakeDamage(int amount)
        {
            health.TakeDamage(amount);
            UIUpdateHealthStats?.Invoke();
        }

        public void Heal(int amount)
        {
            health.Heal(amount);
            UIUpdateHealthStats?.Invoke();
        }

        public int GetCurrentHealthPoints()
        {
            return health.GetPoints();
        }

        public int GetCurrentAttackPowerPoints()
        {
            return attackPower.GetPoints();
        }

        void OnCollisionEnter2D(Collision2D other)
        {
            Ball b = other.gameObject.GetComponent<Ball>();

            if(b == null)
                return;

            Vector2 ballSurface = other.contacts[0].normal;
            Vector2 newDirection = ballSurface * (-1); // shoot it back!
            b.ShootAtDirection(newDirection);
            WiggleSprite?.Invoke();
        }

        // returns false if transaction failed
        public bool BuyAbility(BaseAbility ability)
        {
            int abilityCost = ability.AttackPowerCost;
            if (attackPower.CanSpend(abilityCost))
            {
                attackPower.SpendAP(abilityCost);

                BaseAbility copy = Instantiate(ability);

                if(ability.AbilityType == BaseAbility.Type.Ball)
                    ballAbilityCarrier.AttachAbility(copy);
                else if (ability.AbilityType == BaseAbility.Type.Player)
                    playerAbilityCarrier.AttachAbility(copy);


                UIUpdateAttackPowerStats ? .Invoke();
                return true;
            }

            return false;
        }

        public bool RefundAbility(BaseAbility ability)
        {
            int abilityCost = ability.AttackPowerCost;

            // Attempt to remove the ability from the ball
            if(ballAbilityCarrier.DetachAbility(ability))
            {
                attackPower.RefundAP(abilityCost);
                UIUpdateAttackPowerStats ? .Invoke();
                return true;
            }
            // Attempt to remove the ability from the player
            else if(playerAbilityCarrier.DetachAbility(ability))
            {
                attackPower.RefundAP(abilityCost);
                UIUpdateAttackPowerStats ? .Invoke();
                return true;
            }
            return false;
        }

        private void activatePlayerAbilities()
        {
            playerAbilityCarrier.ActivateAbilities(this);
        }

        // these could be added to a seperate component
        // for example, a class called Consumer.
        public void ConsumeItem(HealthItem item)
        {
            if(item.ApplyTo(health))
            {
                // Debug.Log("player got item: " + item.Name());
                UIUpdateHealthStats?.Invoke();
                OnEventPickingUpHealth?.Invoke();
            }
            
        }

        public void ConsumeItem(AttackPowerItem item)
        {
            
            if(item.ApplyTo(attackPower))
            {
                // Debug.Log("player got item: " + item.Name());
                UIUpdateAttackPowerStats?.Invoke();
                OnEventPickingUpAp?.Invoke();
            }
        }

    }
}
