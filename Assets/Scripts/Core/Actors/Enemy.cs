using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Actors;
using Core.UI;
using Core.Stats;

public class Enemy : Actor
{

    Health Health;

    Coroutine routine;

    void Awake()
    {
        Health = GetComponent<Health>();
    }

    public void TakeDamage(int amount)
    {
        Health.TakeDamage(amount);

        if (routine != null)
            StopCoroutine(routine);

        routine = StartCoroutine(animateDamageTaken());
    }

}
