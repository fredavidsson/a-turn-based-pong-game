using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Stats;
using Core.UI;


namespace Core.Actors
{
[RequireComponent(typeof(Health))]
public class Actor : MonoBehaviour
{   
    // probably could have done these in an interface instead
    public bool IsDoneMoving = true;
    public bool IsDoneRotating = true;

    [SerializeField]
    private float turnSpeed = 25.0f;
    [SerializeField]
    private float moveSpeed = 25.0f;

    Color defaultColor;

    void Start()
    {
        // make a copy of the default color
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        if(sprite != null)
        {
            Color spriteColor = sprite.color;
            defaultColor = new Color(spriteColor.r, spriteColor.g, spriteColor.b, spriteColor.a); 
        }


    }

    public void RotateTo(Vector3 point)
    {
        IsDoneRotating = false; 
        StartCoroutine(rotateTowards(point));
    }

    public void MoveToDestination(Vector3 destination)
    {
        IsDoneMoving = false;
        StartCoroutine(moveTo(destination));
    }

    private IEnumerator moveTo(Vector3 destination)
    {
        // OrdinaryDifferentialEquation ode = new OrdinaryDifferentialEquation(0.1f, 1.0f, -1.0f, (Vector2) transform.position);

        bool moving = true;
        while(moving)
        {
            transform.position = Vector2.MoveTowards(transform.position, destination, moveSpeed * Time.deltaTime);
            // transform.position = ode.Update(Time.deltaTime, (Vector2) destination, transform.position - destination);
            
            
            if(Vector2.Distance(transform.position, destination) < 0.05f)
            {
                // Debug.Log("Done moving!");
                moving = false;
            }

            yield return null;
        }
        IsDoneMoving = true;
        // Debug.Log("Outside while loop!");
        yield return null;
    }

    private IEnumerator rotateTowards(Vector3 point)
    {
        bool rotating = true;
        float time = 0;
        float maxDuration = 0.5f;
        
        while(rotating)
        {
            Vector2 current = transform.right;
            Vector2 direction = (point - transform.position);
            Vector2 rotation = Vector3.RotateTowards(current, direction, Time.deltaTime * turnSpeed, 0.025f); 
            transform.right = rotation;

            // there is a bug that prevents this angle to ever reach this point.
            // Maybe this timer will help?
            if(Vector2.Angle(current, direction) < .05f || time > maxDuration)
            {
                // Debug.Log("done rotating");
                rotating = false;
            }   
            time += Time.deltaTime;
            yield return null;
        }
        IsDoneRotating = true;
        yield return null;
    }

        // take damage 
    protected IEnumerator animateDamageTaken()
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();

        bool shaking = true;
        Vector2 oldPosition = transform.localPosition;

        float t1 = 0f;
        float tMax = 0.3f; // half a second
        sprite.color = Color.red;
        while(shaking)
        {
            
            float x = Random.Range(0.05f, 0.05f);
            float y = Random.Range(0.05f, 0.05f);
            Vector2 location = new Vector2(x, y) + oldPosition;

            transform.localPosition = location;

            sprite.color = Color.Lerp(Color.red, Color.white, t1);
            if(t1 > tMax) // stop shaking when enough time passed
            {
                shaking = false;
            }
            yield return null;
            t1 += Time.deltaTime;
        }
        sprite.color = defaultColor;

        yield return null;
    }
}
}


