## A turn-based pong game
This was an old project that started from learning a couple of design patterns and learning 2D game development in Unity. 

![screenshot](https://gitlab.com/fredavidsson/a-turn-based-pong-game/-/raw/main/screenshot001.jpg)

## How-To-Play
At the start of the game, there is a ghost that appears when the mouse is moving up or down. When left-clicking anywhere in the game using the mouse, the ghost will get stuck in position, this is an indication that you want your paddle to move to this position (you can also undo this by right-clicking anywhere in the game using the mouse). Next, you will notice that the ghost is rotating and facing your mouse position, left-clicking will lock the rotation of your choice. 

When a position and rotation have been set, the menu at the bottom will become interactable. The execute button shoots the square ball in the direction the paddle is facing, but you might want to buy an ability first because without an ability you cannot deal any damage to the enemy (blue square to the right). To buy an ability, just left-click on the Abilities button in the menu at the bottom of the screen (recall that you need to set a position and rotation, otherwise this menu is not interactable). 

When enough damage has been dealt to the enemy, the enemy will go to the second phase. In the second phase, the enemy spawns a rotating square that reflects your ball back to you, however, these squares get destroyed when your ball collides with them.

During the first phase, the enemy has 10 health points. Second phase, the enemy has 25 health points. 

## Download
Windows 10 download link here https://pankie91.itch.io/turn-based-pong-game , requires WinRAR or similar to extract the files. 

## Learning Experience
1. Resolution and scaling problems.
For upcoming projects, I need to focus more on getting scaling work properly with different resolutions, this game was only developed focusing on 1080p monitors. 

2. Follow coding commandments.
 > Thou shalt take time to name things well!

Classes should be nouns (Pirate, Criminal, City, Planet), and avoid things like "Manager", "Controller", etc. And methods should be verbs (Add, Clear, Close, Closed, Died) and more consistent, for example, try to stick with one of the following when getting something: FetchValue, GetValue, or RetriveValue. Not a combination of these. 

Also, the abilities menu consists of check-boxes. In code, these are called radio buttons. They used to be radio buttons but I never changed their names. Definitely should have been changed long ago. 

> Thy classes shall do one thing (functions too).

Increase the readability of the code. 

> Thou shalt not refactor before the 3rd repetition.

Had problems where I choose a design pattern before actually implementing features, is easier to just implement it first and then refactor it later. 

> Thy classes shall be highly cohesive and thy classes shall be loosely coupled.

There are a lot of dependencies from left to right. Gotta focus more on creating code more independent of each other.

> Thou shalt use composition over inheritance!

The actor class is really unnecessary in the project, all it does is move and rotate and be able to run some animations.  These could have been components / other scripts instead. 


## Known bugs or missing features
- Ball can get stuck.
- Player cannot die.
- Player cannot win.
- Does not display damage done to the enemy when he is taking damage.
